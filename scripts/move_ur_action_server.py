#!/usr/bin/env python3

"""This script is a ros action server."""

import rospy
import sys
import moveit_commander
from geometry_msgs.msg import Pose, PoseStamped, Point, Quaternion
import tf_conversions
import actionlib
import sys

from welding_robot_msgs.msg import WeldingPathAction 
from welding_robot_msgs.msg import WeldingPathFeedback
from welding_robot_msgs.msg import WeldingPathGoal
from welding_robot_msgs.msg import WeldingPathResult

from typing import List

# from ros_int_ac_msgs.msg import WeldingPathActionFeedback
# from ros_int_ac_msgs.msg import WeldingPathActionGoal
# from ros_int_ac_msgs.msg import WeldingPathActionResult



class MoveURActionServer:

    _result = WeldingPathResult()
    _fedback = WeldingPathFeedback

    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()
    move_group = moveit_commander.MoveGroupCommander("manipulator")

    def __init__(self, name) -> None:
        print("=== init action server")
        self._action_name = name
        moveit_commander.roscpp_initialize(sys.argv)
        
        self.move_group.set_goal_position_tolerance(1e-4)
        self.move_group.set_max_velocity_scaling_factor(0.01)
        self.move_group.set_max_acceleration_scaling_factor(0.01)

        eef_link = self.move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

                
        self.action_server = actionlib.SimpleActionServer("ur_mover", WeldingPathAction, execute_cb=self.execute_cb, auto_start=False)
        self.action_server.start()

    def cartesian_movement(self, poses:List[PoseStamped]):
        """Plans and executes cartesian movements."""
        waypoints = [p.pose for p in poses]

        end_eff_step = 0.01
        jump_threshld = 0.0

        plan, fraction = self.move_group.compute_cartesian_path(waypoints, end_eff_step, jump_threshld)

        return plan, fraction


    def execute_cb(self, goal):
        plan, fraction = self.cartesian_movement(goal.poses)

        if fraction >= 1.0:
            print("=== Planning was successful, Starting movement execution")
            self.move_group.execute(plan, wait=True)
            self._result.result_code = 1
            self.action_server.set_succeeded(self._result, "Execution finished.")

        else:
            print("=== ERROR: Planning was not successful!")
            self._result.result_code = -1
            self.action_server.set_aborted(self._result, "Planning failed, poses might be out of robot's range.")

if __name__ == "__main__":

    rospy.init_node("move_ur_action_server")
    moveit_commander.roscpp_initialize(sys.argv)

    server = MoveURActionServer(rospy.get_name())
    rospy.spin()
    

