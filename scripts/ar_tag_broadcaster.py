#!/usr/bin/env python3

import rospy
from ar_track_alvar_msgs.msg import AlvarMarker, AlvarMarkers
import tf_conversions
from geometry_msgs.msg import Pose, PoseStamped, TransformStamped, Quaternion, Point

import tf2_ros

from tf2_geometry_msgs import do_transform_point, do_transform_pose

import tf2_geometry_msgs
import math
from copy import deepcopy

import time


HEIGHT_WELDING_NOZZLE_MM = 98

def pose_marker_cb(msg):
    marker_ids = {marker.id: index for index, marker in enumerate(msg.markers)}

    if artag_id not in marker_ids:
        return
    marker = msg.markers[marker_ids.get(artag_id)]
    cur_stamp = marker.header.stamp
    
    pose_stamped = marker.pose
    pose_stamped.header.frame_id = "camera_frame"
    pose_stamped.header.stamp = cur_stamp

    # output pose seems correct
    marker_pose = tf_buffer.transform(pose_stamped, "base_link")
    
    marker_pose.pose.position.z = 0.01
    q = marker_pose.pose.orientation

    _, _, z_rot = tf_conversions.transformations.euler_from_quaternion([q.x, q.y, q.z, q.w])
    q_new = tf_conversions.transformations.quaternion_from_euler(0, 0, z_rot)
    # q_new = tf_conversions.transformations.quaternion_from_euler(math.pi, math.pi, z_rot)
    marker_pose.pose.orientation = Quaternion(*q_new)
    
    t = TransformStamped()
    t.header = marker_pose.header
    t.child_frame_id = "marker_smoothed"
    t.transform.translation.x = marker_pose.pose.position.x
    t.transform.translation.y = marker_pose.pose.position.y
    t.transform.translation.z = marker_pose.pose.position.z
    t.transform.rotation = marker_pose.pose.orientation
    tf_broadcaster.sendTransform(t)



    # get transform of tool0 - nozzle
    t = tf_buffer.lookup_transform("tool0", "nozzle", rospy.Time.now(), rospy.Duration(0.5))
    
    nozzle_height = t.transform.translation.z
    nozzle_offset_y = t.transform.translation.y
    

    t.header.frame_id = "marker_smoothed"
    t.child_frame_id = "tcp_goal"

    tf_broadcaster.sendTransform(t)
    
    tcp_goal = do_transform_pose(marker_pose, t)
    # tcp_goal.header.stamp = rospy.Time.now()

    # tcp_goal = tf_buffer.transform(tcp_goal, "base_link", rospy.Duration(1))
    # print(tcp_goal)
    
    
    # new_pose = do_transform_pose(marker_pose, t)
    # new_pose.header.frame_id = "base_link"


    # t = TransformStamped()
    # t.header = new_pose.header
    # t.child_frame_id = "tcp_goal"
    # t.transform.translation.x = new_pose.pose.position.x
    # t.transform.translation.y = new_pose.pose.position.y
    # t.transform.translation.z = new_pose.pose.position.z

    # t.transform.rotation = new_pose.pose.orientation
    # tf_broadcaster.sendTransform(t)


    # # rotate:
    # t = TransformStamped()
    # t.header.frame_id = "tcp_goal"
    # t.header.stamp = rospy.Time.now()
    # t.child_frame_id = "tcp_goal_rotated"
    # t.transform.translation.x = 0
    # t.transform.translation.y = 0
    # t.transform.translation.z = 0
    # t.transform.rotation = Quaternion(*tf_conversions.transformations.quaternion_from_euler(math.pi, 0, 0))
    # tf_broadcaster.sendTransform(t)



if __name__ == "__main__":
    rospy.init_node("ar_tag_pose_broadcaster")
    tf_buffer = tf2_ros.Buffer(rospy.Duration(5))
    tf_listener = tf2_ros.TransformListener(tf_buffer)
    tf_broadcaster = tf2_ros.TransformBroadcaster()

    time.sleep(0.3)
    

    rospy.Subscriber("/ar_pose_marker", AlvarMarkers, pose_marker_cb)
    artag_id = rospy.get_param(rospy.get_name() + "/ar_tag_id")
    above_surface = rospy.get_param(rospy.get_name() + "/above_surface")


    rospy.spin()