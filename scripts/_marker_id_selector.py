#!/usr/bin/env python3
import rospy
from std_msgs.msg import Int16
# from std_msgs.msg import String

def id_selector():

    rospy.init_node("marker_id_selector")

    current_id =  None

    while True:
        print("Select your marker id you want to track: ... \n")
        input_val = input("Input a id (int starting at 0):   ")
        try:
            input_num = int(input_val)
            if input_num < 0:
                raise Exception("smaller 0")
            current_id = input_num
            break

        except:
            print("Your input was not correct. Try again!")
    
    print("Your selection is: ", current_id)

    pub = rospy.Publisher("marker_id", Int16, queue_size=1)
    rate = rospy.Rate(1)
    while not rospy.is_shutdown():
        rospy.loginfo(f"Publishing: {current_id}")
        pub.publish(current_id)
        rate.sleep()



if __name__ == '__main__':
    try:
        id_selector()
    except rospy.ROSInterruptException as e:
        rospy.loginfo(f"[ERROR] - Caught Exception while executing: {e}")