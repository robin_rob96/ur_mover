#!/usr/bin/env python3

import rospy
from geometry_msgs.msg import PoseStamped, TransformStamped
import tf2_ros


def main():
    """Showcase for using transform broadcaster."""

    br = tf2_ros.TransformBroadcaster()
    rate = rospy.Rate(1)
    
    while not rospy.is_shutdown():
        t = TransformStamped()
        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "ar_marker_9"
        t.child_frame_id = "ar_marker_9_senkrecht"
        t.transform.translation.x = 0.1
        t.transform.translation.y = 0.2
        t.transform.translation.z = 0.3
        t.transform.rotation.w = 0.0
        t.transform.rotation.x = 1.0
        t.transform.rotation.y = 0.0
        t.transform.rotation.z = 0.0
        
        
        br.sendTransform(t)
        rate.sleep()


if __name__ == "__main__":
    rospy.init_node("test_transform")
    main()
    # rospy.spin()