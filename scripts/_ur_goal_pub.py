#!/usr/bin/env python3


import rospy
import geometry_msgs.msg
from std_msgs.msg import Int16
from ar_track_alvar_msgs.msg import AlvarMarker, AlvarMarkers

import tf2_geometry_msgs
import sys


marker_id = None
pose = None



def create_goal_pose(pose_, frame_id = "camera_frame"):
    target_pose = tf2_geometry_msgs.PoseStamped()
    target_pose.pose = pose_

    scale = 0.5
    target_pose.pose.position.x = scale * pose_.position.x
    target_pose.pose.position.y = scale * pose_.position.y
    target_pose.pose.position.z = scale * pose_.position.z

    target_pose.header.frame_id = frame_id
    target_pose.header.stamp = rospy.Time.now()

    return target_pose



def received_marker_id(msg):
    # rospy.loginfo(f"received marker id: {msg.data}")
    global marker_id
    marker_id = int(msg.data)
    

def received_poses(msg):
    # rospy.loginfo("Received something ... %s", data)
    num_markers = len(msg.markers)

    # ids = [m.id for m in msg.markers]
    
    ids = {}
    for ind, m in enumerate(msg.markers):
        ids[m.id] = ind

    if marker_id == None:
        rospy.loginfo("There is no marker id published to look out for at /marker_id")
        return

    if not marker_id in ids:
        rospy.loginfo(f"No pose with id {marker_id} found at /ar_pose_marker")
        return

    # got pose estimation
    marker = msg.markers[ids.get(marker_id)]

    header = marker.header
    id_ = marker.id
    confidence = marker.confidence
    stamped_pose = marker.pose

    p = stamped_pose.pose

    # add offset to pose etc.

    global pose
    pose = p
    
    


def main():
    rospy.init_node("ur_goal_publisher")
    rospy.Subscriber("/ar_pose_marker", AlvarMarkers, received_poses)
    rospy.Subscriber("/marker_id",Int16, received_marker_id)

    pose_pub = rospy.Publisher("/ur_goal_pose", tf2_geometry_msgs.PoseStamped, queue_size=1)
    rate = rospy.Rate(1)

    while not rospy.is_shutdown():
        if pose == None: continue

        rospy.loginfo("publishing goal pose at /ur_goal_pose")
        
        stamped_pose = create_goal_pose(pose, frame_id="camera_frame")
        pose_pub.publish(stamped_pose)
        rate.sleep()
    
    rospy.spin()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException as e:
        rospy.loginfo(f"[ERROR] - Caught Exception while executing: {e}")