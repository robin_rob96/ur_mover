#!/usr/bin/env python3

"""Script for moving robot in a 'default' - position. 

JUST USE IT FOR SIMULATED ROBOTS!
"""

import rospy
import moveit_commander
import math
import sys



def execute_joint_goal_default_pose(group):
    # We can get the joint values from the group and adjust some of the values:
    joint_goal = group.get_current_joint_values()
    joint_goal[0] = -math.pi
    joint_goal[1] = -math.pi/2
    joint_goal[2] = -math.pi/2
    joint_goal[3] = -math.pi/2
    joint_goal[4] = math.pi/2
    joint_goal[5] = math.pi

    # The go command can be called with joint values, poses, or without any
    # parameters if you have already set the pose or joint target for the group
    group.go(joint_goal, wait=True)

    # Calling ``stop()`` ensures that there is no residual movement
    group.stop()


if __name__ == "__main__":

    rospy.init_node("homing_node")
    moveit_commander.roscpp_initialize(sys.argv)
    move_group = moveit_commander.MoveGroupCommander("manipulator")
    execute_joint_goal_default_pose(move_group)
