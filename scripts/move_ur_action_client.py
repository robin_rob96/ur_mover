#!/usr/bin/env python3

import rospy
import actionlib
import math
import tf2_ros
import sys
import moveit_commander
import tf_conversions

from geometry_msgs.msg import PoseStamped, Point, Quaternion
from geometry_msgs.msg import TransformStamped

from welding_robot_msgs.msg import WeldingPathAction 
from welding_robot_msgs.msg import WeldingPathActionFeedback
from welding_robot_msgs.msg import WeldingPathActionGoal
from welding_robot_msgs.msg import WeldingPathActionResult
from welding_robot_msgs.msg import WeldingPathFeedback
from welding_robot_msgs.msg import WeldingPathGoal
from welding_robot_msgs.msg import WeldingPathResult
# from ros_int_ac_msgs.msg import WeldingPathAction 
# from ros_int_ac_msgs.msg import WeldingPathActionFeedback
# from ros_int_ac_msgs.msg import WeldingPathActionGoal
# from ros_int_ac_msgs.msg import WeldingPathActionResult
# from ros_int_ac_msgs.msg import WeldingPathFeedback
# from ros_int_ac_msgs.msg import WeldingPathGoal
# from ros_int_ac_msgs.msg import WeldingPathResult


TARGET_TCP_FRAME = "tcp_goal"

def get_pose(x,y,z):
    pose1 = PoseStamped()
    pose1.header.stamp = rospy.Time.now()
    pose1.header.frame_id = "base_link"
    p = Point(x,y,z)
    pose1.pose.position = p

    quat = tf_conversions.transformations.quaternion_from_euler(math.pi,0,0)
    q = Quaternion(*quat)
    pose1.pose.orientation = q
    return pose1


def create_pose(t:TransformStamped):
    ...

    pose = PoseStamped()
    # pose.header.stamp = rospy.Time.now()
    pose.header.frame_id = "base_link"
    pose.pose.position.x = t.transform.translation.x
    pose.pose.position.y = t.transform.translation.y
    pose.pose.position.z = t.transform.translation.z
    pose.pose.orientation = t.transform.rotation
    return pose


def pose_reachable(pose):
    """Plans for 1 specific pose to verify that it's reachable by the robot arm."""
    move_group.set_pose_target(pose)
    try:
        success, *args = move_group.plan()
        rospy.loginfo(f"Pose reachable: {success}")
    except Exception as e:
        rospy.logerr(e)
        return False
    return success



def main():
    ...

    poses_stamped = []

    while True:
        input_str = input("Press ENTER to add pose. Type x to stop teaching.\n")
        input_str = input_str.lower()

        if "x" in input_str:
            break

        try:
            trans = tf_buffer.lookup_transform("base_link", TARGET_TCP_FRAME, rospy.Time.now(), timeout=rospy.Duration(0.5))
        except Exception as e:
            rospy.logerr("Seems like the AR Tag was not detected by the camera.")
            continue

        pose = create_pose(trans)

        if not pose_reachable(pose):
            ...
            rospy.logerr("Pose cannot be added because it's out of range.")
            continue


        poses_stamped.append(pose)
        rospy.loginfo("=== Pose added.")

        
    # execute pose list
    if len(poses_stamped) == 0:
        rospy.logerr("There was no AR Tag pose captured. Client will shutdown.")
        return


    rospy.logwarn(f"Total of {len(poses_stamped)} poses added. Press ENTER to start planning & execution.")
    input("Press ENTER to start movement.")
    client.wait_for_server()

    goal = WeldingPathGoal(poses=poses_stamped, goal_description = "This is my goal description")
    client.send_goal(goal)

    client.wait_for_result()

    result = client.get_result()
    rospy.loginfo(f"result of action server: {result}")


if __name__== "__main__":
    
    rospy.init_node("move_ur_action_client")
    moveit_commander.roscpp_initialize(sys.argv)
    move_group = moveit_commander.MoveGroupCommander("manipulator")

    tf_buffer = tf2_ros.Buffer(rospy.Duration(5))
    tf_listener = tf2_ros.TransformListener(tf_buffer)

    client = actionlib.SimpleActionClient("ur_mover", WeldingPathAction)
    
    
    main()

