#!/usr/bin/env python3


# from __future__ import print_function
# from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import tf
import time

import tf2_geometry_msgs

from math import pi, tau, dist, fabs, cos
from std_msgs.msg import String


pose_stamped = None


def received_goal_pose(msg):
    ...
    global pose_stamped
    pose_stamped = msg
    print("received pose: ", type(pose_stamped))



def pub_trajectory(robot, plan):
    ...
    # import pdb
    # pdb.set_trace()


    display_trajectory = moveit_msgs.msg.DisplayTrajectory()
    display_trajectory.trajectory_start = robot.get_current_state()
    display_trajectory.trajectory.append(robot.get_current_state())
    display_trajectory.trajectory.append(plan)

    display_trajectory_publisher.publish(display_trajectory)


def move_to_pose(move_group, robot, pose_target):

    move_group.set_goal_position_tolerance(1e-2)
    move_group.set_pose_target(pose_target)

    plan_success, plan, planning_time, error_code = move_group.plan()
    # pub_trajectory(robot, plan)
    print("Planned ...", plan_success, planning_time, error_code)


    input(f"The goal has been planned in {planning_time} seconds. Press Enter do start the movement!")


    success = move_group.go(wait=True)

    if success == True:
        print("--- go() was successfull ")
    else:
        print("--- ERROR: go() was not successfull ")
    move_group.clear_pose_targets()
    






def get_target_pose(x=-0.5, y=0.5, z=0.2, ee_z_down = True):

    target_pose = tf2_geometry_msgs.PoseStamped()

    pose= geometry_msgs.msg.Pose()
    pose.position.x = x
    pose.position.y = y
    pose.position.z = z

    if ee_z_down:
        pose.orientation.x = 0.0
        pose.orientation.y = 1.0
        pose.orientation.z = 0.0
        pose.orientation.w = 0.0

    target_pose.pose = pose
    target_pose.header.frame_id = "camera_frame"
    target_pose.header.stamp = rospy.Time.now()

    return target_pose

if __name__ == "__main__":


    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

    robot = moveit_commander.RobotCommander()
    scene = moveit_commander.PlanningSceneInterface()

    group_name = "manipulator"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    display_trajectory_publisher = rospy.Publisher(
        "/move_group/display_planned_path",
        moveit_msgs.msg.DisplayTrajectory,
        queue_size=20,
    )

    robot = moveit_commander.RobotCommander()

    # rospy.Subscriber("/ur_goal_pose",tf2_geometry_msgs.PoseStamped, received_goal_pose)

    print("=== STARTING NOW")
    time.sleep(1)

    print("=== WAITING FOR MSG /ur_goal_pose for 15 seconds ....")
    pose_stamped = rospy.wait_for_message("/ur_goal_pose", tf2_geometry_msgs.PoseStamped, timeout=15)

    # pose_stamped.pose.orientation.x = 0
    # pose_stamped.pose.orientation.y = 0
    # pose_stamped.pose.orientation.z = 0

    print("GOT MY POSE", type(pose_stamped))


    # target_pose = get_target_pose(0.1,0,0.1, ee_z_down = False)
    # move_to_pose_cartesian_path(move_group, target_pose)
    
    
    move_to_pose(move_group, robot, pose_stamped)
    # rospy.spin()