#!/usr/bin/env python3

import rospy

from ar_track_alvar_msgs.msg import AlvarMarker, AlvarMarkers
import tf2_ros

import tf2_geometry_msgs
from tf2_geometry_msgs import do_transform_pose

from geometry_msgs.msg import PoseStamped, TransformStamped

import tf_conversions

import geometry_msgs.msg 
import moveit_commander
import sys

import time
import copy
import math


AR_TAG_ID = 9
GLUEING_PLANE_Z_OFFSET = 0.4

ar_tag_pose = None




def __init__():
    moveit_commander.roscpp_initialize(sys.argv)
    robot =  moveit_commander.RobotCommander()
    group_name = "manipulator"
    group = moveit_commander.MoveGroupCommander(group_name)

    rospy.init_node("glueing_path_teacher")
    rospy.Subscriber("/ar_pose_marker", AlvarMarkers, received_markers)
    ar_tag_pose = None # geometry_msgs.msg.PoseStamped


def received_markers(msg):
    """Callback method. Subscribes to /ar_pose_marker """
    global ar_tag_pose
    marker_ids = {marker.id: index for index, marker in enumerate(msg.markers)}
    if AR_TAG_ID in marker_ids:
        marker = msg.markers[marker_ids.get(AR_TAG_ID)]
        pose = marker.pose
        pose.header.frame_id = "camera_frame"
        pose.header.stamp = rospy.Time.now()

        pose_transformed = transform_pose(pose, "base_link")
        ar_tag_pose = pose_transformed
        broadcast_goal_pose(ar_tag_pose)


def transform_pose(pose_stamped:geometry_msgs.msg.PoseStamped, target_frame:str, fix_orn = False)-> PoseStamped:
    tf_buffer = tf2_ros.Buffer()
    tf2_ros.TransformListener(tf_buffer)
    
    transform = tf_buffer.lookup_transform(target_frame, pose_stamped.header.frame_id, rospy.Time(0), rospy.Duration(1.0))
    transformed_pose = tf2_geometry_msgs.do_transform_pose(pose_stamped, transform)
    transformed_pose.pose.position.z = GLUEING_PLANE_Z_OFFSET

    if fix_orn:
        # set orientation in a way thats alligned with ee default orn
        q = tf_conversions.transformations.quaternion_from_euler(math.pi, 0, 0)
        # q = tf_conversions.transformations.quaternion_from_euler(0, math.pi, math.pi/2)
        orn = geometry_msgs.msg.Quaternion(*q)
        transformed_pose.pose.orientation = orn
    else:
        # orientation from ar tag, but z axis is parallel to base_link's z
        # q = transformed_pose.pose.orientation
        # _, _, z_ = tf_conversions.transformations.euler_from_quaternion([q.x, q.y, q.z, q.w])
        # q_new = tf_conversions.transformations.quaternion_from_euler(0, 0, z_)
        # transformed_pose.pose.orientation = geometry_msgs.msg.Quaternion(*q_new)

        q = transformed_pose.pose.orientation

        _, _, z_rot = tf_conversions.transformations.euler_from_quaternion([q.x, q.y, q.z, q.w])
        q_new = tf_conversions.transformations.quaternion_from_euler(math.pi, 0, z_rot)
        transformed_pose.pose.orientation = geometry_msgs.msg.Quaternion(*q_new)

    return transformed_pose
    

def broadcast_goal_pose(pose:geometry_msgs.msg.PoseStamped):
    """Broadcast transform to see it in rviz"""
    broadcaster = tf2_ros.TransformBroadcaster()
    t = TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = "base_link"
    t.child_frame_id = "ar_pose_transformed"
    
    # pos = pose.pose.position
    t.transform.translation.x = pose.pose.position.x
    t.transform.translation.y = pose.pose.position.y
    t.transform.translation.z = pose.pose.position.z

    # allign orientation with 'default' ee orientation
    # q = tf_conversions.transformations.quaternion_from_euler(0, math.pi, math.pi/2)
    # t.transform.rotation.x = q[0]
    # t.transform.rotation.y = q[1]
    # t.transform.rotation.z = q[2]
    # t.transform.rotation.w = q[3]


    t.transform.rotation.x = pose.pose.orientation.x
    t.transform.rotation.y = pose.pose.orientation.y
    t.transform.rotation.z = pose.pose.orientation.z
    t.transform.rotation.w = pose.pose.orientation.w

    broadcaster.sendTransform(t)


def pose_is_valid(group, pose):


    return True
    ...

def teaching_poses(group):
    
    poses = []
    while True:
        inp_val = input("Place the AR Tag to the desired position and press ENTER. Stop teaching with 'x' + ENTER \n")
        
        if "x" in inp_val or "X" in inp_val:
            print(f"Teaching stopped. Captured {len(poses)} poses.")
            break
        
        if ar_tag_pose == None:
            raise Exception("Pose is None, check camera/ ar_alvar setup!")

        
        if pose_is_valid(group, ar_tag_pose):
            poses.append(ar_tag_pose)
            print("--- Valid pose added.")
        else:
            print("Invalid pose: Pose not reachable. Try again! ")


    return poses


def execute_joint_goal_default_pose(group):
    # We can get the joint values from the group and adjust some of the values:
    joint_goal = group.get_current_joint_values()
    joint_goal[0] = -math.pi
    joint_goal[1] = -math.pi/2
    joint_goal[2] = -math.pi/2
    joint_goal[3] = -math.pi/2
    joint_goal[4] = math.pi/2
    joint_goal[5] = 0

    # The go command can be called with joint values, poses, or without any
    # parameters if you have already set the pose or joint target for the group
    group.go(joint_goal, wait=True)

    # Calling ``stop()`` ensures that there is no residual movement
    group.stop()



def execute_cartesian_path_tutorial(group):
    scale = 1.0
    waypoints = []

    wpose = group.get_current_pose().pose
    wpose.position.z -= scale * 0.1  # First move up (z)
    wpose.position.y += scale * 0.2  # and sideways (y)
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.x += scale * 0.1  # Second move forward/backwards in (x)
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.y -= scale * 0.1  # Third move sideways (y)
    waypoints.append(copy.deepcopy(wpose))

    # We want the Cartesian path to be interpolated at a resolution of 1 cm
    # which is why we will specify 0.01 as the eef_step in Cartesian
    # translation.  We will disable the jump threshold by setting it to 0.0 disabling:
    (plan, fraction) = group.compute_cartesian_path(
                                    waypoints,   # waypoints to follow
                                    0.01,        # eef_step
                                    0.0)         # jump_threshold

    # Note: We are just planning, not asking move_group to actually move the robot yet:

    if fraction >= 1.0:
        print("=== PLANNING WAS SUCCESSFUL")
        time.sleep(1)

        group.execute(plan, wait=True)
    else:
        print("=== PLANNING WAS NOT SUCCESSFUL")




def plan_cartesian_path(group, stamped_poses:list):
    start_pose = group.get_current_pose().pose

    waypoints = [start_pose] + [pose_stamped.pose for pose_stamped in stamped_poses]

    eef_step = 0.01
    jump_threshold = 0.0
    plan, fraction = group.compute_cartesian_path(waypoints, eef_step, jump_threshold)
    return plan, fraction
    



def execute_trajectory(group, plan):
    input("Press Enter to execute the planned trajectory: \n")
    print("=== EXECUTING TRAJECTORY ===")
    retVal = group.execute(plan, wait=True)
    print("=== FINISHED EXECUTING ===")


def main():
    ...
    moveit_commander.roscpp_initialize(sys.argv)
    robot =  moveit_commander.RobotCommander()
    group_name = "manipulator"
    group = moveit_commander.MoveGroupCommander(group_name)

    execute_joint_goal_default_pose(group)
    poses = teaching_poses(group)

    plan, fraction = plan_cartesian_path(group, poses)
    
    if fraction >= 1.0:
        print("=== PLANNED CARTESIAN PATH")
        execute_trajectory(group, plan)
    else:
        print("=== PLANNING FAILED!!!")
    # rospy.spin()





if __name__ == "__main__":


    rospy.init_node("glueing_path_teacher")
    rospy.Subscriber("/ar_pose_marker", AlvarMarkers, received_markers)

    main()